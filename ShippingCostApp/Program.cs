﻿using System;

namespace ShippingCostApp
{
    internal sealed class Program
    {
        private static void Main()
        {
            var amount = new PaymentAmount();

            Console.WriteLine(amount.Amount(1000, 3, true));
        }
    }
}