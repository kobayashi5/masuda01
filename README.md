# 現場で役立つシステム設計の原則 復習問題その一
復習問題は二つあります。好きなほうをやってみよう!

| 復習問題 | 説明 |
| ---- | ---- |
| [ShippingCostApp](./ShippingCostApp) | 単価と数量から送料や消費税を加算します。このコードをリファクタリングしてみましょう! |
| [ValueObject](./ValueObject) | 郵便番号 Value Object を作ってみよう! |
